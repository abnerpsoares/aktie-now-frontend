import React from 'react';
import { Container } from 'react-bootstrap';

import Layout from './_Layout';

export default class NotFound extends React.Component {

    render() {

        return(
            <Layout>

                <Container className="mt-4">
                    <h2 className="mb-4">Opsss.</h2>
                    <h5 className="mb-4">Parece que você está perdido. <a href="/">Clique aqui</a> para voltar para a página incial.</h5>
                </Container>

            </Layout>
        );

    }
    
}