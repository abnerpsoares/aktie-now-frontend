import React from 'react';
import api from '../../utils/api';
import { CardDeck, Spinner } from 'react-bootstrap';

import Book from './Book';

export default class BookList extends React.Component {

    constructor(){

        super();

        this.state = {
            bookList: null
        };

    }

    componentDidMount() {
        this.getBookList();
    }

    getBookList = async () => {
        
        const list  = await api.get('/book');
        const bookList = list.data.books;
        
        this.setState({
            bookList
        });

    }

    getBookComponentList = () => {

        const books = this.state.bookList;
        let listContent = [];
        
        if( books !== null ){
            if( books.length > 0 ){

                books.map(function( book ){
                    
                    listContent.push(
                        <Book
                            data={ book } 
                            key={ `bk-${ book._id }` }
                        />
                    );

                    return true;

                });

            }else{
                return <h6>Nenhum livro encontrado.</h6>;
            }
        }else{
            return <Spinner animation="border" variant="warning" className="mt-4" />;
        }

        return <CardDeck>
                <ul>
                    { listContent }
                </ul>
               </CardDeck>;

    }

    render() {
        return this.getBookComponentList();
    }
    
}