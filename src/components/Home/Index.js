import React from 'react';
import { Jumbotron, Container } from 'react-bootstrap';

import Layout from './_Layout';
import BookList from './BookList';

export default class Index extends React.Component {

    render() {

        return(
            <Layout>

                <Jumbotron fluid>
                    <Container>
                        <h1>Compartilhe Sua Experiência</h1>
                        <p>
                            Ajude novos leitores a encontrarem o próximo livro perfeito para degustar.
                        </p>
                    </Container>
                </Jumbotron>

                <Container>
                    <h4 className="mb-4">Livros Disponíveis:</h4>
                    <BookList />
                </Container>

            </Layout>
        );

    }
    
}