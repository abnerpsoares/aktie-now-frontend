import React from 'react';
import { Modal, Button, Form, Container, Row, Col, Image, Spinner, Alert } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';

import api from '../../utils/api';
import config from '../../config/app';

export default class RatingModal extends React.Component {

    constructor(){

        super();

        this.state = {
            showModal: true,
            rating: 2.5,
            personName: '',
            savingData: false,
            success: false
        }

    }

    closeModal = () => {
        this.setState({ showModal: false });
        this.props.closeParentModal();
    }

    setRating = ( rating ) => {
        this.setState({ rating });
    }

    setPersonName = (event) => {
        this.setState({ personName: event.target.value });
    }

    submitRating = (event) => {

        this.setState({
            savingData: true
        });

        this.saveRating();

        event.preventDefault();
    }

    saveRating = async () => {

        const book = this.props.data;
        const saveRating = await api.post('/book/rating', {
            bookId: book._id,
            person_name: this.state.personName,
            description: '',
            stars: this.state.rating
        });

        if( saveRating.status === 200 ){

            this.setState({
                success: true
            });

            setTimeout(() => {
                window.location.reload(false);
            }, 2000);

        }else{
            alert('Erro ao salvar a avaliação. Por favor, tente novamente.');
        }

        this.setState({
            savingData: false
        });

    }

    render() {

        const book = this.props.data;

        return(
            <Modal
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={ this.state.showModal }
                onHide={ () => this.closeModal() }
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        { book.name }
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                <Container>
                    <Row>
                        <Col className="col-auto">
                            <Image src={`${ config.apiUrl }/book/cover/${ book._id }`} thumbnail />
                        </Col>
                        <Col>
                            <h6><i>{ `${ book.release_year } - ${ book.author }` }</i></h6>
                            <p>
                                { `"${ book.summary }"` }
                            </p>
                        </Col>
                    </Row>
                </Container>
                </Modal.Body>
                <Modal.Footer className="justify-content-start">

                    <Form className="w-100" onSubmit={ this.submitRating }>

                        { this.state.success &&
                            <Alert variant="success">
                                Avaliação enviada com sucesso. Obrigado ;)
                            </Alert>
                        }

                        <h4 className="mb-4">Enviar Avaliação:</h4>

                        <Form.Group controlId="formName">
                            <Form.Label>Nome:</Form.Label>
                            <Form.Control type="text" placeholder="Por favor, informe seu nome" required onChange={ this.setPersonName } />
                            <Form.Text className="text-muted">
                                Fique tranquilo, nós não iremos publicá-lo ;)
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formRating" className="mt-4">
                            <Form.Label>Avaliação:</Form.Label>
                            <Form.Text>
                                <StarRatings
                                    rating={ this.state.rating }
                                    starDimension="25px"
                                    starSpacing="5px"
                                    starRatedColor="rgb(255,201,0)"
                                    changeRating={ this.setRating }
                                />
                            </Form.Text>
                        </Form.Group>

                        {
                            !this.state.savingData ?
                                <Button variant="success" className="mt-4" type="submit">
                                    Enviar
                                </Button>
                            :
                            <Spinner animation="border" variant="warning" className="mt-4" />
                        }

                    </Form>
                    
                </Modal.Footer>
            </Modal>
        );

    }
    
}