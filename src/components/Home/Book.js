import React from 'react';
import { Card, Button } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';
import RatingModal from './RatingModal';

import config from '../../config/app';
import rating from '../../utils/rating';

export default class Book extends React.Component {

    constructor(){

        super();

        this.state = {
            modal: false,
            book: []
        };

    }

    closeModal() {
        this.setState({ modal: false });
    }

    render() {

        const book = this.props.data;

        return(

            <>

                { this.state.modal &&
                    <RatingModal
                        data={ this.state.book }
                        show={ true }
                        closeParentModal={ this.closeModal.bind(this) }
                    />
                }

                <li>
                <Card bg="light" text="dark">
                    <Card.Img variant="top" src={`${ config.apiUrl }/book/cover/${ book._id }`} alt={ book.name } />
                    <Card.Body>
                        <Card.Title>{ book.name }</Card.Title>
                        <Card.Text>
                            { `${ book.release_year } - ${ book.author }` }
                        </Card.Text>
                    </Card.Body>
                    <Card.Footer className="text-center">

                        <StarRatings
                            rating={ rating.getStarts( book.ratings ) }
                            starDimension="15px"
                            starSpacing="5px"
                            starRatedColor="rgb(255,201,0)"
                        />

                        <Button variant="dark" className="mt-3" onClick={ () => this.setState({ modal: true, book }) }>Avaliar</Button>

                    </Card.Footer>

                </Card>
                </li>

            </>

        );

    }
    
}