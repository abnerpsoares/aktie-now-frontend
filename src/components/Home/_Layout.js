import React from 'react';
import { Navbar, Nav, Form, Button, Container } from 'react-bootstrap';

import logo from '../../assets/images/logo.png';
import '../../assets/style.css';

export default class Layout extends React.Component {

    render() {

        return(
            <>
                <Navbar className="topbar">
                    <Container>
                        <Navbar.Brand href="/">
                            <img
                                src={ logo }
                                className="d-inline-block align-top"
                                alt="AKTIElib."
                            />
                        </Navbar.Brand>
                        <Nav className="mr-auto">

                        </Nav>
                        <Form inline>
                            <Button variant="dark" onClick={ () => window.location.href = '/admin' }>Admin</Button>
                        </Form>
                    </Container>
                </Navbar>

                { this.props.children }

            </>
        );

    }
    
}