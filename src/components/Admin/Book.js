import React from 'react';
import { Form, Button, Spinner, Image, ButtonToolbar } from 'react-bootstrap';

import api from '../../utils/api';
import config from '../../config/app';

import Layout from './_Layout';

export default class Book extends React.Component {

    constructor(){

        super();

        this.state = {
            pageTitle: 'Novo Livro',
            pageSubtitle: 'Preencha os campos abaixo para adicionar um novo livro.',
            btnLabel: 'Cadastrar',

            savingData: false,

            bookId: null,
            bookName: '',
            bookAuthor: '',
            bookReleaseYear: '',
            bookSummary: '',
            bookImage: ''
        }

    }

    componentDidMount(){
        
        const bookId = window.location.pathname.split('/admin/book/')[1];

        if( bookId !== undefined ){
            this.getBookDetails( bookId );
        }

    }

    getBookDetails = async ( bookId ) => {

        const bookRequest = await api.get(`/book/${ bookId }`);
        const bookData = bookRequest.data.book; 

        this.setState({
            bookId: bookData._id,
            bookName: bookData.name,
            bookAuthor: bookData.author,
            bookReleaseYear: bookData.release_year,
            bookSummary: bookData.summary,

            pageTitle: 'Editar Livro',
            pageSubtitle: 'Preencha os campos abaixo para editar o novo livro.',
            btnLabel: 'Editar'
        });
        
    }

    setBookName = (event) => {
        this.setState({ bookName: event.target.value });
    }

    setBookAuthor = (event) => {
        this.setState({ bookAuthor: event.target.value });
    }

    setBookReleaseYear = (event) => {
        this.setState({ bookReleaseYear: event.target.value });
    }

    setBookSummary = (event) => {
        this.setState({ bookSummary: event.target.value });
    }

    setBookImage = (event) => {
        this.setState({ bookImage: event.target.files[0] });
    }

    submitBook = (event) => {

        this.setState({
            savingData: true
        });
    
        this.saveBook();
    
        event.preventDefault();
    }
    
    saveBook = async () => {

        if( this.state.bookId === null ){
            this.insertBook();
        }else{
            this.updateBook();
        }

    }

    insertBook = async () => {

        const data = new FormData();
        data.append('name', this.state.bookName);
        data.append('author', this.state.bookAuthor);
        data.append('release_year', this.state.bookReleaseYear);
        data.append('summary', this.state.bookSummary);
        data.append('cover_image', this.state.bookImage);

        await api.post('/book', data)
        .then(response => { 
            
            alert('Livro cadastrado com sucesso!');
            window.location.href = '/admin/book-list';

        })
        .catch(error => {
            alert(error.response.data.error);
        });

    }

    updateBook = async () => {

        const data = new FormData();
        data.append('name', this.state.bookName);
        data.append('author', this.state.bookAuthor);
        data.append('release_year', this.state.bookReleaseYear);
        data.append('summary', this.state.bookSummary);

        if( this.state.bookImage !== '' ){
            data.append('cover_image', this.state.bookImage);
        }

        await api.put(`/book/${ this.state.bookId }`, data)
        .then(response => { 
            
            alert('Livro alterado com sucesso!');
            window.location.href = '/admin/book-list';

        })
        .catch(error => {
            alert(error.response.data.error);
        });

    }

    render() {

        return(
            <Layout>

                <h4>{ this.state.pageTitle }</h4>
                <p className="mb-4">{ this.state.pageSubtitle }</p>

                <Form onSubmit={ this.submitBook }>
                    
                    <Form.Group>
                        <Form.Label>Nome</Form.Label>
                        <Form.Control type="text" placeholder="Informe o nome do livro" value={ this.state.bookName } required onChange={ this.setBookName } />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Autor</Form.Label>
                        <Form.Control type="text" placeholder="Informe o autor do livro" value={ this.state.bookAuthor } required onChange={ this.setBookAuthor } />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Ano de Lançamento</Form.Label>
                        <Form.Control type="number" placeholder="Informe o ano de lançamento do livro" value={ this.state.bookReleaseYear } required onChange={ this.setBookReleaseYear } />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Sinopse</Form.Label>
                        <Form.Control as="textarea" rows="5" placeholder="Informe a sinopse do livro" value={ this.state.bookSummary } onChange={ this.setBookSummary } />
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Imagem</Form.Label>
                        <Form.Control type="file" required={ this.state.bookId !== null ? false : true  } onChange={ this.setBookImage } />
                    </Form.Group>

                    <a href={ `${ config.apiUrl }/book/cover/${ this.state.bookId }` } target="_blank" rel="noopener noreferrer">
                        <Image src={`${ config.apiUrl }/book/cover/${ this.state.bookId }`} rounded width={100} />
                    </a>
                    
                    <ButtonToolbar className="mt-5">
                        {
                            !this.state.savingData ?
                                <Button variant="dark" type="submit">
                                    { this.state.btnLabel }
                                </Button>
                            :
                                <Spinner animation="border" variant="warning" className="mt-4" />
                        }
                    </ButtonToolbar>
                    
                </Form>

            </Layout>
        );

    }
    
}