import React from 'react';
import { Table, ButtonGroup, Button, Container, Row, Image } from 'react-bootstrap';

import api from '../../utils/api';
import config from '../../config/app';

import Layout from './_Layout';

export default class BookList extends React.Component {

    constructor(){

        super();

        this.state = {
            bookList: []
        }

        this.deleteBookConfirm = this.deleteBookConfirm.bind(this);

    }

    componentDidMount() {
        this.getBookList();
    }

    getBookList = async () => {
        
        const list  = await api.get('/book');
        const bookList = list.data.books;
        
        this.setState({
            bookList
        });

    }

    deleteBook = async ( bookId ) => {

        await api.delete(`/book/${ bookId }`)
        .then(response => { 

            alert('Livro excluído com sucesso!');
            window.location.href = '/admin/book-list';

        })
        .catch(error => {
            alert(error.response.data.error);
        });;

    }

    deleteBookConfirm = ( book ) => {
        if( window.confirm(`Deseja excluir o livro ${ book.name }?`) === true ){
            this.deleteBook( book._id );
        }
    }

    getBookComponentList = () => {

        const books = this.state.bookList;
        let listContent = [];
        
        if( books.length > 0 ){

            books.map( book => {
                
                listContent.push(
                    <tr key={ book._id }>
                        <td>{ book.name }</td>
                        <td>{ book.author }</td>
                        <td>{ book.release_year }</td>
                        <td>
                            <a href={ `${ config.apiUrl }/book/cover/${ book._id }` } target="_blank" rel="noopener noreferrer">
                                <Image src={`${ config.apiUrl }/book/cover/${ book._id }`} rounded width={100} />
                            </a>
                        </td>
                        <td>
                            <ButtonGroup aria-label="">
                                <Button variant="info" onClick={ () => window.location.href = `/admin/book/${ book._id }` }>Editar</Button>
                                <Button variant="danger" onClick={ () => this.deleteBookConfirm( book ) }>Excluir</Button>
                            </ButtonGroup>
                        </td>
                    </tr>
                );

                return true;

            });

        }else{
            return <tr><td colSpan="3">Nenhum livro encontrado.</td></tr>;
        }

        return listContent;

    }

    render() {

        return(
            <Layout>

                <h4>Livros</h4>
                <p className="mb-4">Gerencie os livros do site.</p>

                <Container>
                    <Row className="justify-content-end">
                        <Button variant="dark" className="mb-4" onClick={ () => window.location.href = '/admin/book' }>+ Novo Livro</Button>
                    </Row>
                </Container>

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Livro</th>
                            <th>Autor</th>
                            <th>Ano de Lançamento</th>
                            <th>Imagem</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.getBookComponentList() }
                    </tbody>
                </Table>

            </Layout>
        );

    }
    
}