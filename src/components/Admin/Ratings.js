import React from 'react';
import { Table } from 'react-bootstrap';
import StarRatings from 'react-star-ratings';

import api from '../../utils/api';
import rating from '../../utils/rating';

import Layout from './_Layout';

export default class Ratings extends React.Component {

    constructor(){

        super();

        this.state = {
            bookList: []
        }

    }

    componentDidMount() {
        this.getBookList();
    }

    getBookList = async () => {
        
        const list  = await api.get('/book');
        const bookList = list.data.books;
        
        this.setState({
            bookList
        });

    }

    getBookComponentList = () => {

        const books = this.state.bookList;
        let listContent = [];
        
        if( books.length > 0 ){

            books.map(function( book ){
                
                listContent.push(
                    <tr key={`bk-${ book._id }`}>
                        <td>{ book.name }</td>
                        <td>{ book.ratings.length }</td>
                        <td>
                            <StarRatings
                                rating={ rating.getStarts( book.ratings ) }
                                starDimension="15px"
                                starSpacing="5px"
                                starRatedColor="rgb(255,201,0)"
                            />
                        </td>
                    </tr>
                );

                return true;

            });

        }else{
            return <tr><td colSpan="3">Nenhum livro encontrado.</td></tr>;
        }

        return listContent;

    }

    render() {

        return(
            <Layout>

                <h4>Avaliações</h4>
                <p className="mb-4">Acompanhe as avaliações dos livros.</p>

                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>Livro</th>
                            <th>Qtde. Avaliações</th>
                            <th>Estrelas</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.getBookComponentList() }
                    </tbody>
                </Table>

            </Layout>
        );

    }
    
}