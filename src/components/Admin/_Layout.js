import React from 'react';
import { Navbar, Nav, Container, Form, Button } from 'react-bootstrap';

import token from '../../utils/token';

import logo from '../../assets/images/logo-admin.png';

import '../../assets/style.css';

export default class Layout extends React.Component {

    logout = () => {
        token.forget();
        window.location.href = '/admin';
    }

    render() {

        return(
            <>
                <Navbar className="topbar-admin" bg="dark" variant="dark">
                    <Container>
                        <Navbar.Brand href="/admin">
                            <img
                                src={ logo }
                                className="d-inline-block align-top"
                                alt="AKTIElib."
                            />
                        </Navbar.Brand>
                        { token.get() !== null &&
                            <Nav className="mr-auto">
                                <Nav.Link href="/admin/ratings">Avaliações</Nav.Link>
                                <Nav.Link href="/admin/book-list">Lista de Livros</Nav.Link>
                                <Nav.Link onClick={ () => this.logout() }>Sair</Nav.Link>
                            </Nav>
                        }
                        <Form inline>
                            <Button variant="warning" onClick={ () => window.location.href = '/' }>Site</Button>
                        </Form>
                    </Container>
                </Navbar>

                <Container className="mt-4">

                    { this.props.children }

                </Container>

            </>
        );

    }
    
}