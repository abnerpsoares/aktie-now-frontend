import React from 'react';
import { Spinner, Form, Button } from 'react-bootstrap';

import api from '../../utils/api';
import token from '../../utils/token';

import Layout from './_Layout';

export default class Login extends React.Component {

    constructor(){

        super();

        this.state = {
            user: '',
            password: '',
            savingData: false
        }

    }

    setUser = (event) => {
        this.setState({ user: event.target.value });
    }

    setPassword = (event) => {
        this.setState({ password: event.target.value });
    }

    submitLogin = (event) => {

        this.setState({
            savingData: true
        });

        this.AuthUser();

        event.preventDefault();
    }

    AuthUser = async () => {

        await api.post('/auth/authenticate', {
            user: this.state.user,
            password: this.state.password
        })
        .then(response => { 

            token.set( response.data.token );

            window.location.href = '/admin/ratings';

        })
        .catch(error => {
            alert(error.response.data.error);
        });

        this.setState({
            savingData: false
        });

    }

    render() {

        return(
            <Layout>

                <h4>Login</h4>
                <p className="mb-4">Informe seu usuário e senha para ter acesso ao sistema.</p>

                <Form onSubmit={ this.submitLogin }>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Usuário</Form.Label>
                        <Form.Control type="text" placeholder="Informe o usuário" required onChange={ this.setUser } />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Senha</Form.Label>
                        <Form.Control type="password" placeholder="Informe a senha" required onChange={ this.setPassword } />
                    </Form.Group>
                    {
                        !this.state.savingData ?
                            <Button variant="dark" type="submit">
                                Logar
                            </Button>
                        :
                            <Spinner animation="border" variant="info" className="mt-4" />
                    }
                </Form>

            </Layout>
        );

    }
    
}