const rating = {

    getStarts: function( ratings ) {

        var starts = 0;

        if( ratings.length > 0 ){

            ratings.map( function(rate){
                starts += rate.stars;
                return true;
            });

            starts = starts / ratings.length;
        }

        return starts;
    }
    
}

export default rating;