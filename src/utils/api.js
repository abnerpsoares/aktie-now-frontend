import axios from "axios";
import config from '../config/app';
import token from './token';

const sendToken = token.get() !== null ? { headers: { "Authorization" : `Bearer ${ token.get() }` } } : {};

export default axios.create({
  baseURL: config.apiUrl,
  responseType: "json",
  ...sendToken
});