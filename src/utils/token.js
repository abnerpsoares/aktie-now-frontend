const tokenIdentifier = "tokenAktieLib";

const token = {

    get: function() {
        return localStorage.getItem( tokenIdentifier );
    },
    
    set: function( token ) {
        localStorage.setItem( tokenIdentifier, token );
    },

    forget: function() {
        localStorage.clear();
    }
    
}

export default token;