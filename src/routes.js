import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import token from './utils/token';

import Index from './components/Home/Index';
import NotFound from './components/Home/NotFound';

import Login from './components/Admin/Login';
import Ratings from './components/Admin/Ratings';
import BookList from './components/Admin/BookList';
import Book from './components/Admin/Book';

const AdminRoute = ({ component: Component, ...rest }) => (    
    <Route
        { ...rest }
        render={ props => (
            token.get() == null
                ?
                    <Component { ...props } />
                :
                    <Redirect to={{ pathname: '/admin/ratings', state: { from: props.location } }} />
        )}
    />
);

const PrivateRoute = ({ component: Component, ...rest }) => (    
    <Route
        { ...rest }
        render={ props => (
            token.get() !== null
                ?
                    <Component { ...props } />
                :
                    <Redirect to={{ pathname: '/admin', state: { from: props.location } }} />
        )}
    />
);

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={ Index } />
            <AdminRoute exact path='/admin' component={ Login } />
            <PrivateRoute exact path="/admin/ratings" component={Ratings} />
            <PrivateRoute exact path="/admin/book-list" component={BookList} />
            <PrivateRoute path="/admin/book" component={Book} />
            <Route path='*' component={ NotFound } />
        </Switch>
    </BrowserRouter>
);

export default Routes;