# AKTIElib. - Frontend

Frontend do sistema de avaliações de livros desenvolvido em Reactjs + Bootstrap consumindo APIs Restful em Nodejs.

# Instalação:
  - O bom e velho ```npm install``` haha.

# Features:
  - Visualizar os livros;
  - Avaliar os livros;
  - Fazer login no módulo administrativo;
  - CRUD dos livros com upload de imagens;
  - Visualizar avaliações de todos os livros cadastrados.

# Demo:

  - Página inicial: http://18.229.79.206
  - Módulo admin: http://18.229.79.206/admin 
  -     Usuário: aktienow - Senha: aktie123


Valeuuu ;)